clear all;
close all;

TMax = 50; % 10 krokow czasowych
n = 10; % Dlugosc pretu
T = zeros(TMax,n);
b = 0.3;

for t = 1:TMax-1,
    T(t,1) = sin(t/pi) * 0.5;
    T(t,n) = cos(t/pi);
end

for t = 1: TMax-1,
  for i=2:n-1,  
    T(t+1,i) = T(t,i) + b * (T(t,i+1) - 2*T(t,i) + T(t,i-1)); 
  end
end

x = 1:n;
y = 1:TMax;
surf(x,y,T)