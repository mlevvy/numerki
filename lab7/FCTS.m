clear all;
close all;

TMax = 50; % 10 krokow czasowych
n = 10; % Dlugosc pretu
T = zeros(TMax,n);
b = 0.3;
T(:,1) = 4;
T(:,10) = 10;


for t = 1: TMax-1,
  for i=2:n-1,
    T(t+1,i) = T(t,i) + b * (T(t,i+1) - 2*T(t,i) + T(t,i-1))   
  end
end

x = 1:n;
y = 1:TMax;
surf(x,y,T)