function x = Jacobi(A,b)
%http://en.wikipedia.org/wiki/Jacobi_method
n = size(A,1);
maxIter = 100;
x = zeros(n,maxIter+1);
tolerance = 0.01;

for k=1:maxIter
    
    for i=1:n,
        sigma = 0;
        for j = 1:n
            if(j~=i)
               sigma = sigma + A(i,j)*x(j,k);
            end
        end
        x(i,k+1)=(b(i)-sigma)/A(i,i);
    end
    diff = abs(x(:,k)-x(:,k+1));
    if diff < tolerance
        k
        x = x(:,k);
        return
    end
    k = k + 1;
end
x = x(:,k);
end