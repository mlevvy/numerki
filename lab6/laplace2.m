n = 100;

A = zeros(n,n);
A(1,:) = 0;
A(:,1) = 0;
A(n,:) = -7
for sinpos = 1:n,
    A(n,sinpos) = sin(sinpos/10);
end
iterMax = 1000;
A

for iter = 1 : iterMax,
    Aold = A;
    A(2:n-1,2:n-1) = (Aold(2-1:n-1-1 ,2:n-1) + Aold(2+1:n-1+1,2:n-1) + Aold(2:n-1,2+1:n-1+1) + Aold(2:n-1,2-1:n-1-1))/4;
end

x = 1:n
y = 1:n
surf(x,y,A)