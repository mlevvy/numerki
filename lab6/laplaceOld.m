clear all;
close all;

n = 10;

A = zeros(n,n);
A(1,:) = 4;
A(:,1) = 8;
iterMax = 1000;
A
Liniowo = zeros(n*n);
LiniowoIndex = 1;


%Zamiana na wektor
for i = 1 : n ,
       for j = 1 : n,
           Liniowo(LiniowoIndex) = A(i,j);
           LiniowoIndex=LiniowoIndex+1;
       end,
end,

%Olbiczenie
for iter = 1 : iterMax,
     LiniowoOld = Liniowo;
     for i = 2 : n -1,
        for j = 2 : n -1,
            index = i*n + j;
            Liniowo(index) = (LiniowoOld(index + 1) + LiniowoOld(index - 1) + LiniowoOld(index - n) + LiniowoOld(index + n))/4;
        end,
     end,
     A;
end
 
%Zamiana na macierz
for i = 1 : n ,
       for j = 1 : n,
           index = i*n + j;
           A(i,j) = Liniowo(index);
       end,
end,

x = 1:n
y = 1:n
surf(x,y,A)