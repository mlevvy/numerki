n = 10;

A = zeros(n,n);
iterMax = 10000;
A

klewo = 10;
kprawo = 0;
kgora = 0;
kdol = 0;

A(1,:) = 10;
A(:,1) = 0;
A(n,:) = 0;
A(:,n) = 0;

for iter = 1 : iterMax,
    Aold = A;
    for i = 2 : n -1,
       for j = 2 : n -1,
           A(i,j) = (Aold(i-1,j) + Aold(i+1,j) + Aold(i,j+1) + Aold(i,j-1))/4;
       end,
    end,
    A;
end

x = 1:n
y = 1:n
figure(2)
surf(x,y,A)