clear all;
close all;

n = 100;
npol=50;

A = zeros(n,n);
A(1,1:npol) = 20;
A(1,npol:n) = 3;
A(n,1:npol) = 18;
A(n,npol:n) = 5;
A(:,1)=17;
A(:,n)=4;

E1 = 0.1;
E2 = 0.2

 iterMax = 1000;
 A

 for iter = 1 : iterMax,
     Aold = A;
     for i = 2 : n -1,
        for j = 2 : npol,
            A(i,j) = (Aold(i-1,j) + Aold(i+1,j) + Aold(i,j+1) + Aold(i,j-1))/4;
        end,
     end,
     
     for i = 2 : n -1,
        for j = npol : n -1,
            A(i,j) = (Aold(i-1,j) + Aold(i+1,j) + Aold(i,j+1) + Aold(i,j-1))/4;
        end,
     end,
     
     A;
 end
 
 x = 1:n
 y = 1:n
 surf(x,y,A)