n = 10;

A = zeros(n,n);
iterMax = 10000;
A

% klewo = 2;
% kprawo = 3;
% kgora = 4;
% kdol = 5;

klewo = 10;
kprawo = 0;
kgora = 0;
kdol = 0;

for iter = 1 : iterMax,
    Aold = A;
    for i = 1 : n,
       for j = 1 : n,
           if i == 1,
               rlewo = klewo;
           elseif i == n
               rprawo = kprawo;
           else
               rlewo = Aold(i-1,j);
               rprawo = Aold(i+1,j);
           end
         
           
           if j == 1,
               rgora = kgora;
           elseif j ==n 
               rdol = kdol;
           else    
               rgora = Aold(i,j+1);
               rdol = Aold(i,j-1);
           end
           
           A(i,j) = (rlewo + rprawo + rgora + rdol)/4;
       end,
    end,
    A;
end

x = 1:n
y = 1:n
figure(1)
surf(x,y,A)