e = 0.001;

disp('###### Bez prekondycjonera:')
A = [2,1;5,7];
b = [11;13];
x = [0;0];

TaPoLewo(A,b,x,e);


disp('######Jacobi prekondycjoner:')
A = [2,1;5,7];
b = [11;13];
x = [0;0];
A = diag(diag(A))^-1*A
TaPoLewo(A,b,x,e);

disp('###### mac1 Bez prekondycjonera:')
A = [1,1/2,1/3;
    1/2,1/3,1/4
    1/3,1/4,1/5];
b = [11;13;15];
x = [0;0;0];

TaPoLewo(A,b,x,e);


disp('###### mac1 Jacobi prekondycjoner:')
A = [1,1/2,1/3;
    1/2,1/3,1/4
    1/3,1/4,1/5];
b = [11;13;15];
x = [0;0;0];
A = diag(diag(A))^-1*A
TaPoLewo(A,b,x,e);


