function x = SORomega(A,b,w)
%http://en.wikipedia.org/wiki/Successive_over-relaxation
n = size(A,1);
maxIter = 18;
x = zeros(n,1);
xLast = zeros(n,1);
tolerance = 0.01;

for k=1:maxIter
    xLast = x;
    for i=1:n,
        sigma = 0;
        for j = 1:n
            if(j~=i)
               sigma = sigma + A(i,j)*xLast(j);
            end
        end
        x(i)=(1-w)*xLast(i) + (w/A(i,i))*(b(i)-sigma);
    end
    k = k + 1;
       
    diff = abs(xLast-x);
    if diff < tolerance
        k;
        return
    end
end
end