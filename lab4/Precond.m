%Bez prekondycjonera. Hilberta. 
A = [1,1/2,1/3;
    1/2,1/3,1/4
    1/3,1/4,1/5];
b = [11;13;15];
x = [0;0;0];
disp('Wspolczynnik uwarunkowania macierzy: ')
cond(A)
disp('10 iteracji bez prekondycjonera: ')
x = ConjGrad2(A,b,x)

%Z prekondycjonerem Jacobiego:
A = diag(diag(A))^-1*A
disp('Wspolczynnik uwarunkowania macierzy z prekondycjonerem Jacobiego: ')
cond(A)
disp('10 iteracji z prekondycjonerem: ')
x = ConjGrad2(A,b,x)

%Dane z poprzedniej macierzy:
[A,b] = mac_1
disp('Dane mac_1. Wspolczynnik uwarunkowania macierzy: ')
cond(A)
disp('Dane mac_1. Promien spektralny: ')
Spectral(A)
A = diag(diag(A))^-1*A
disp('Dane mac_1. Prekondycjoner Jacobiego. Wspolczynnik uwarunkowania macierzy: ')
cond(A)
disp('Dane mac_1. Prekondycjoner Jacobiego. Promien spektralny: ')
Spectral(A)





