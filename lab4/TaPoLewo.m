function [x,i] = TaPoLewo(A,b,x,e)
disp('Wspolczynnik uwarunkowania macierzy: ')
cond(A)
disp('Promien spektralny: ')
Spectral(A)

i=1;
flag = true;
while flag 
     xprev = x;
     v = - ((A * xprev) -b);
     x = xprev + e * v;
     i=i+1;
     if x - xprev < e
         flag = false;
     end    
end
     
disp('Wynik: ')
x
disp('Ilosc iteracji')
i
        
    
end