x0 = 1;
h = 0.1;

x = -pi:.1:pi;
j = 1;
for i = -pi:.1:pi
y(j) = FunkcjaA(i);
j = j + 1;
end

plot(x,y)


x1 = x0 + h;
wPrzod = (FunkcjaA(x1)-FunkcjaA(x0))/h
x2 = x0 - h;
wTyl = (FunkcjaA(x0)-FunkcjaA(x2))/h
centralna = (FunkcjaA(x1)-FunkcjaA(x2))/2*h
druga = (FunkcjaA(x1) + FunkcjaA(x2) - 2 * FunkcjaA(x0))/2*h


%Metoda druga
x = 1:1:7;
x0 = 4;
w = 1;

sum11 = 0;
for x = 1:1:7
    sum11 = (x0 - x)*w;
end

sum12 = 0;
for x = 1:1:7
    sum12 = ((x0 - x)^3*w^2)/2;
end

sum21 = 0;
for x = 1:1:7
    sum21 = (x0 - x)^3*w^2;
end

sum22 = 0;
for x = 1:1:7
    sum22 = ((x0 - x)^4*w^3)/4;
end

A = [sum11,sum12; sum21, sum22];

b11 = 0;
for x = 1:1:7
    b11 = (x0 - x)*w^2 * (FunkcjaA(x)-FunkcjaA(x0));
end

b12 = 0;
for x = 1:1:7
    b12 = ((x0 - x)^2*w^2)/2 * (FunkcjaA(x)-FunkcjaA(x0));
end

b = [b11; b12];

result = A\b

