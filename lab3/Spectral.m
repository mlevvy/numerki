function x = Spectral(A)
D = diag(diag(A));
L = tril(A);
U = triu(A);

B = -(L +D)^-1*U;
x = max(abs(eig(B)));
end