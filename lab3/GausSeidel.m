function x = GausSeidel(A,b)
%http://pl.wikipedia.org/wiki/Metoda_Gaussa-Seidla
n = size(A,1);
maxIter = 100;
x = zeros(n,maxIter+1);
tolerance = 0.01;

for k=2:maxIter
    %check if convergence is reached
    for i=1:n,
        sigma = 0;
        for j = 1:i-1
           sigma = sigma + A(i,j)*x(j,k);
        end
        for j = i+1:n
           sigma = sigma + A(i,j)*x(j,k-1);
        end
        x(i,k)=(b(i)-sigma)/A(i,i);
    end
    diff = abs(x(:,k)-x(:,k+1));
    if diff < tolerance
        k
        x = x(:,k);
        return
    end
    k = k + 1;
end
x = x(:,maxIter);
end