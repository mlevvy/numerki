clear all;
close all;

a = magic(2)
b = [0;12]
epsilon = 0.0000000001
figure(1)
MetodaPotegowa(a,b,epsilon)


a = magic(6)
b = [0;0;0;1;0;1]
epsilon = 0.0000000001
figure(2)
MetodaPotegowa(a,b,epsilon)

a = hilb(6)
b = [0;0;0;1;0;1]
epsilon = 0.0000000001
figure(3)
MetodaPotegowa(a,b,epsilon)

%Norm zwykla
figure(4);
MetodaRA(magic(2),10);
figure(5);
MetodaRA(magic(3),10);
figure(6);
MetodaRA(magic(4),10);
figure(7);
MetodaRA(magic(5),10);

%Norm frobeniusa
N = 10;
a = magic(2);
figure(8);
MetodaRAF(a,N);
