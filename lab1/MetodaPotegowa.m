function maxEig = MetodaPotegowa(A,b, epsilon)

% Z = [1,1]';
% A = [2,-2;-2,5];

% Z1 = A*Z
% Sigma = (Z1'*A*Z1)/(Z1'*Z1)

 wyznacznik = det(A)
 uwarunkowanie = cond(A)
 wartosciWlasne = eig(A)

Z = b;
sigma(1)=1000;
sigma(2)=0;

i = 2;
while(abs(sigma(i-1) - sigma(i)) > epsilon),
    Z = A * Z;
    Z = Z / norm(Z);
    sigma(i+1) = (Z'*A*Z)/(Z'*Z);
    i=i+1;
end
maxEig = sigma(i);


plot(sigma(3:end))