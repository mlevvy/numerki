% WYMIARY MACIERZY
N=20;
M=40;
X=N*M;

A=zeros(X,X);
z=ones(X,1);
h=1/(N-1);

xi=[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
%yi=h*[0:1:X-1];

% Bi Xi
bi=0;
ksi=0;

% R�WNANIE T ORAZ WARUNKI BRZEGOWE DLA T
T = 1/h^2*(4*diag(ones(N,1))-diag(ones(N-1,1),-1)-diag(ones(N-1,1),1));
T1 = 1/h^2*(4*diag(ones(N,1))-diag(ones(N-1,1),-1)-diag(ones(N-1,1),1));

T(1,1:2) = [1/h^2 0];
T1(1,1:2) = T(1,1:2);
for i=2:N-1
    T(i,i-1) = -1/h^2;
    T(i,i) = 4/h^2;
    T(i,i+1) = -1/h^2;
    T1(i,i-1) = -1/h^2;
    T1(i,i) = 1/h^2;
    T1(i,i+1) = -1/h^2;    
end

T(end,end-2:end)=1/h^2*[-1 2 1];
T1(end,end-2:end)=1/h^2*[-1 2 -2];
T3 = T1;
I=-1/h^2*eye(N);
I(1,1) = 0;
I1 = I*2;
I4 = I1;
I2 = -I;
I5 = I2;

% G��WNA P�TLA PROGRAMU
for i=1:M
    if i==1
        A(1:N,1:N)=T1;
        A(1:N,N+1:2*N)=I1;
        A(1:N,2*N+1:3*N)=I2;
    elseif i==M
        A(end-N+1:end,end-N+1:end)=T3;
        A(end-N+1:end,end-2*N+1:end-N)=I4;
        A(end-N+1:end,end-3*N+1:end-2*N)=I5;
    else
        A(1+(i-1)*N:i*N,1+(i-1)*N:i*N)=T;
        A(1+(i-1)*N:i*N,1+(i-2)*N:(i-1)*N)=I;
        A(1+(i-1)*N:i*N,1+i*N:(i+1)*N)=I;
    end
end

% WEKTOR B
b=zeros(X,1);

for i=17:25
    b((i-1)*N+1) = xi(i-16); 
end

% RYSOWANIE WYKRES�W
y=gmres(A,b,20,1e-9,150);
%y=A\b;
figure

ui=reshape(y,N,M);
mesh(ui)

figure
contour(ui,20)
