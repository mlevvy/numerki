n = 100;

A = zeros(n,n);
A(1,:) = 4;
A(:,1) = 8;
A(n,:) = -7
iterMax = 1000;
A

for iter = 1 : iterMax,
    Aold = A;
    for i = 2 : n -1,
       for j = 2 : n -1,
           A(i,j) = (Aold(i-1,j) + Aold(i+1,j) + Aold(i,j+1) + Aold(i,j-1))/4;
       end,
    end,
    A;
end

x = 1:n
y = 1:n
surf(x,y,A)